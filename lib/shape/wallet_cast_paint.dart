import 'package:flutter/material.dart';

class WalletCastPaint extends CustomPainter {
  final Color surfaceColor;

  WalletCastPaint({required this.surfaceColor});
  @override
  void paint(Canvas canvas, Size size) {
    double width = size.width;
    double height = size.height;

    double radius = width * 0.07;

    double cutOutHeight = height * 0.25;

    Path path = Path();

    path.moveTo(radius, cutOutHeight);
    path.arcToPoint(Offset(0, cutOutHeight + radius),
        radius: Radius.circular(radius), clockwise: false);
    path.lineTo(0, height - radius);
    path.arcToPoint(Offset(radius, height),
        radius: Radius.circular(radius), clockwise: false);
    path.lineTo(width - radius, height);
    path.arcToPoint(Offset(width, height - radius),
        radius: Radius.circular(radius), clockwise: false);

    path.lineTo(width, radius);

    path.arcToPoint(Offset(width - radius, 0),
        radius: Radius.circular(radius), clockwise: false);

    path.lineTo(width * 0.7 + radius / 2, 0);

    path.arcToPoint(Offset(width * 0.7 - radius / 2, cutOutHeight * 0.3),
        radius: Radius.circular(radius), clockwise: false);

    path.lineTo(width * 0.6, cutOutHeight - radius / 2);

    path.arcToPoint(Offset(width * 0.6 - radius, cutOutHeight),
        radius: Radius.circular(radius * 2), clockwise: true);

    path.lineTo(radius, cutOutHeight);

    Paint paint = Paint()
      ..style = PaintingStyle.fill
      ..color = surfaceColor;

    canvas.drawPath(path, paint);
  }

  @override
  bool shouldRepaint(covariant CustomPainter oldDelegate) {
    return true;
  }
}
