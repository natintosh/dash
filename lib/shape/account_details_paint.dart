import 'package:flutter/material.dart';

class AccountDetailsPaint extends CustomPainter {
  final Color surfaceColor;

  AccountDetailsPaint({required this.surfaceColor});

  @override
  void paint(Canvas canvas, Size size) {
    Path path = Path();

    double height = size.height;
    double width = size.width;

    double radius = width * 0.07;

    double cutOutHeight = height - height * 0.9;

    path.moveTo(radius, 0);
    path.arcToPoint(Offset(0, radius),
        radius: Radius.circular(radius), clockwise: false);
    path.lineTo(0, height - radius);
    path.arcToPoint(Offset(radius, height),
        radius: Radius.circular(radius), clockwise: false);

    path.lineTo(width * 0.45 - radius / 2, height);

    path.arcToPoint(
        Offset(width * 0.45 + radius / 2, height * 0.9 + cutOutHeight * 0.7),
        radius: Radius.circular(radius),
        clockwise: false);

    path.lineTo(width * 0.55 - radius / 4, height * 0.9 + cutOutHeight * 0.3);

    path.arcToPoint(Offset(width * 0.55 + radius / 2, height - cutOutHeight),
        radius: Radius.circular(radius), clockwise: true);

    path.lineTo(width - radius, height * 0.9);
    path.arcToPoint(Offset(width, (height * 0.9) - radius),
        radius: Radius.circular(radius), clockwise: false);
    path.lineTo(width, radius);
    path.arcToPoint(Offset(width - radius, 0),
        radius: Radius.circular(radius), clockwise: false);
    path.lineTo(radius, 0);

    Paint paint = Paint()
      ..color = surfaceColor
      ..style = PaintingStyle.fill;
    canvas.drawPath(path, paint);
  }

  @override
  bool shouldRepaint(covariant CustomPainter oldDelegate) {
    return true;
  }
}
