import 'package:flutter/material.dart';

class WalletPaint extends CustomPainter {
  final Color surfaceColor;

  WalletPaint({required this.surfaceColor});
  @override
  void paint(Canvas canvas, Size size) {
    double width = size.width;
    double height = size.height;

    double radius = width * 0.07;

    double cutOutHeight = height * 0.25;

    Path path = Path();

    path.moveTo(radius, 0);
    path.arcToPoint(Offset(0, radius),
        radius: Radius.circular(radius), clockwise: false);
    path.lineTo(0, height - radius);
    path.arcToPoint(Offset(radius, height),
        radius: Radius.circular(radius), clockwise: false);
    path.lineTo(width - radius, height);
    path.arcToPoint(Offset(width, height - radius),
        radius: Radius.circular(radius), clockwise: false);

    path.lineTo(width, cutOutHeight + radius);

    path.arcToPoint(Offset(width - radius, cutOutHeight),
        radius: Radius.circular(radius), clockwise: false);

    path.lineTo(width * 0.7 + radius / 2, cutOutHeight);

    path.arcToPoint(
      Offset(width * 0.7 - radius / 2, cutOutHeight * 0.7),
      radius: Radius.circular(radius * 2),
      clockwise: true,
    );

    path.lineTo(width * 0.6, radius / 2);

    path.arcToPoint(Offset(width * 0.6 - radius, 0),
        radius: Radius.circular(radius), clockwise: false);

    path.lineTo(radius, 0);

    Paint paint = Paint()
      ..style = PaintingStyle.fill
      ..color = surfaceColor;

    canvas.drawPath(path, paint);
  }

  @override
  bool shouldRepaint(covariant CustomPainter oldDelegate) {
    return true;
  }
}
