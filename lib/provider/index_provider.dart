import 'package:flutter/cupertino.dart';

class IndexProvider extends ChangeNotifier {
  int _index = 2;

  int get index => _index;

  set index(int value) {
    _index = value;
    notifyListeners();
  }
}
