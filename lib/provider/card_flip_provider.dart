import 'package:flutter/cupertino.dart';

enum AnimDirection { forward, backward }

class CardFlipProvider extends ChangeNotifier {
  AnimDirection _direction = AnimDirection.forward;
  int _index = 0;
  int _animatingIndex = 0;

  AnimDirection get direction => _direction;

  set direction(AnimDirection value) {
    _direction = value;
    notifyListeners();
  }

  int get index => _index;

  set index(int value) {
    _index = value;
    notifyListeners();
  }

  int get animatingIndex => _animatingIndex;

  set animatingIndex(int value) {
    _animatingIndex = value;
    notifyListeners();
  }

  List<String> get data => ['BTC', 'NAT', 'AAA', 'XYZ'];
}
