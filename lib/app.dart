import 'package:dash_demo/pages/index.dart';
import 'package:dash_demo/provider/card_flip_provider.dart';
import 'package:dash_demo/provider/index_provider.dart';
import 'package:dash_demo/utils/themes.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:provider/provider.dart';

class DashApp extends StatelessWidget {
  const DashApp({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    SystemChrome.setEnabledSystemUIMode(SystemUiMode.edgeToEdge);

    SystemChrome.setSystemUIOverlayStyle(
      const SystemUiOverlayStyle(
        statusBarColor: Colors.transparent,
        statusBarBrightness: Brightness.dark,
        statusBarIconBrightness: Brightness.light,
        systemNavigationBarColor: Colors.transparent,
        systemNavigationBarIconBrightness: Brightness.light,
        systemStatusBarContrastEnforced: true,
      ),
    );

    return MaterialApp(
      debugShowCheckedModeBanner: false,
      title: 'DASH Crypto Wallet',
      initialRoute: '/',
      onGenerateRoute: onGenerateRoute,
      darkTheme: AppTheme.darkTheme,
      themeMode: ThemeMode.dark,
      builder: (context, child) {
        return MultiProvider(
          providers: [
            ChangeNotifierProvider(
              create: (context) {
                return IndexProvider();
              },
            ),
            ChangeNotifierProvider(
              create: (context) {
                return CardFlipProvider();
              },
            ),
          ],
          child: Scaffold(
            extendBody: true,
            body: child,
          ),
        );
      },
    );
  }

  Route? onGenerateRoute(RouteSettings settings) {
    Widget page = Container();

    switch (settings.name) {
      case '/':
        page = const IndexPage();
        break;
      default:
    }

    return MaterialPageRoute(
      builder: (context) => page,
      settings: settings,
      maintainState: true,
      fullscreenDialog: false,
    );
  }
}
