import 'dart:developer';

import 'package:dash_demo/pages/screens/components/account_details_card.dart';
import 'package:dash_demo/utils/colors.dart';
import 'package:dash_demo/utils/widget_view.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:rive/rive.dart';
import 'package:sized_context/sized_context.dart';

class AccountsScreen extends StatefulWidget {
  const AccountsScreen({Key? key}) : super(key: key);

  @override
  _AccountsScreenState createState() => _AccountsScreenState();
}

class _AccountsScreenState extends State<AccountsScreen> {
  Color color = const Color(0xFF353746).withOpacity(0.5);

  late PageController? pageViewController;

  VoidCallback get onBackButtonPressed => () {};

  VoidCallback get onInfoButtonPressed => () {};

  ValueChanged<bool> get onSwitchChanged => (bool value) {};

  void pageControllerListener() {
    setState(() {
      current = pageViewController!.page!.floor() + 1;
    });
  }

  int current = 2;

  @override
  void initState() {
    pageViewController = PageController(initialPage: 1, viewportFraction: 0.85)
      ..addListener(pageControllerListener);
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return _AccountScreenView(this);
  }
}

class _AccountScreenView
    extends WidgetView<AccountsScreen, _AccountsScreenState> {
  const _AccountScreenView(_AccountsScreenState state) : super(state);

  @override
  Widget build(BuildContext context) {
    return Column(
      children: [
        const SizedBox(height: 4),
        Row(
          children: [
            const SizedBox(width: 16),
            IconButton(
              onPressed: state.onBackButtonPressed,
              icon: const Icon(Icons.arrow_back),
              color: AppColors.secondary,
            ),
            const Expanded(
              child: Text(
                'My Accounts',
                textAlign: TextAlign.center,
              ),
            ),
            IconButton(
                onPressed: state.onBackButtonPressed,
                icon: const Icon(
                  Icons.add_circle_outline_rounded,
                  color: AppColors.secondary,
                )),
            const SizedBox(width: 16),
          ],
        ),
        Expanded(
          child: PageView(
            controller: state.pageViewController,
            children: [
              AccountDetailsCard(
                onInfoButtonPressed: state.onInfoButtonPressed,
                onSwitchChanged: state.onSwitchChanged,
                primaryOn: false,
              ),
              AccountDetailsCard(
                onInfoButtonPressed: state.onInfoButtonPressed,
                onSwitchChanged: state.onSwitchChanged,
                primaryOn: true,
              ),
              AccountDetailsCard(
                onInfoButtonPressed: state.onInfoButtonPressed,
                onSwitchChanged: state.onSwitchChanged,
                primaryOn: false,
              ),
            ],
          ),
        ),
        Indicator(
          current: state.current,
        ),
      ],
    );
  }
}

class Indicator extends StatefulWidget {
  final int current;
  const Indicator({Key? key, required this.current}) : super(key: key);

  @override
  _IndicatorState createState() => _IndicatorState();
}

class _IndicatorState extends State<Indicator> {
  Artboard? artboard;

  final String stateMachineName = 'Indicator State';

  StateMachineController? stateMachineController;

  late SimpleAnimation oneToTwo, twoToOne, threeToTwo, twoToThree, current;
  void initializeRiveAnimation() async {
    try {
      final ByteData data = await rootBundle.load('assets/rive/indicator.riv');

      final RiveFile riveFile = RiveFile.import(data);

      final Artboard artboard = riveFile.mainArtboard;

      oneToTwo = SimpleAnimation('OneToTwo');
      twoToOne = SimpleAnimation('TwoToOne');
      threeToTwo = SimpleAnimation('ThreeToTwo');
      twoToThree = SimpleAnimation('TwoToThree');

      current = oneToTwo;

      artboard.addController(current);

      setState(() {
        this.artboard = artboard;
      });
    } on Exception catch (e) {
      log('$e');
    }
  }

  @override
  void initState() {
    initializeRiveAnimation();
    super.initState();
  }

  @override
  void didUpdateWidget(covariant Indicator oldWidget) {
    super.didUpdateWidget(oldWidget);
    if (oldWidget.current != widget.current) {
      artboard!.removeController(current);
      if (widget.current == 1 && oldWidget.current == 2) {
        artboard!.addController(twoToOne);
        setState(() {
          current = twoToOne;
        });
      } else if (widget.current == 2 && oldWidget.current == 1) {
        artboard!.addController(oneToTwo);
        setState(() {
          current = oneToTwo;
        });
      } else if (widget.current == 2 && oldWidget.current == 3) {
        artboard!.addController(threeToTwo);
        setState(() {
          current = threeToTwo;
        });
      } else if (widget.current == 3 && oldWidget.current == 2) {
        artboard!.addController(twoToThree);
        setState(() {
          current = twoToThree;
        });
      }
    }
  }

  @override
  Widget build(BuildContext context) {
    return SizedBox(
        width: context.widthPct(0.2),
        height: 10,
        child: artboard == null
            ? Container()
            : Rive(
                artboard: artboard!,
              ));
  }
}
