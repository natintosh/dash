import 'dart:math';

import 'package:dash_demo/utils/colors.dart';
import 'package:dash_demo/utils/widget_view.dart';
import 'package:flutter/material.dart';
import 'package:google_fonts/google_fonts.dart';
import 'package:intl/intl.dart';
import 'package:sized_context/sized_context.dart';

class WalletCard extends StatefulWidget {
  const WalletCard({Key? key}) : super(key: key);

  @override
  _WalletCardState createState() => _WalletCardState();
}

class _WalletCardState extends State<WalletCard> {
  @override
  Widget build(BuildContext context) {
    return _WalletCardView(this);
  }
}

class _WalletCardView extends WidgetView<WalletCard, _WalletCardState> {
  const _WalletCardView(_WalletCardState state) : super(state);

  @override
  Widget build(BuildContext context) {
    return Container(
      width: context.widthPct(0.85),
      height: context.widthPct(0.55),
      clipBehavior: Clip.antiAlias,
      decoration: ShapeDecoration(
        shape: RoundedRectangleBorder(
          borderRadius: BorderRadius.circular(context.widthPct(0.85) * 0.11),
        ),
      ),
      child: Stack(
        children: [
          SizedBox(
            width: context.widthPct(0.85),
            height: context.widthPct(0.55),
            child: Container(
              decoration: ShapeDecoration(
                shape: RoundedRectangleBorder(
                    borderRadius:
                        BorderRadius.circular(context.widthPct(0.55) * 0.11)),
                color: AppColors.surface,
              ),
            ),
          ),
          Positioned(
            bottom: -50,
            right: -50,
            child: Container(
              width: context.widthPct(0.30),
              height: context.widthPct(0.30),
              decoration: BoxDecoration(
                shape: BoxShape.circle,
                color: Colors.grey.withOpacity(0.05),
              ),
            ),
          ),
          Positioned(
            bottom: -50,
            right: -50,
            child: Container(
              width: context.widthPct(0.40),
              height: context.widthPct(0.40),
              decoration: BoxDecoration(
                shape: BoxShape.circle,
                color: Colors.grey.withOpacity(0.05),
              ),
            ),
          ),
          Positioned(
            bottom: -50,
            right: -50,
            child: Container(
              width: context.widthPct(0.60),
              height: context.widthPct(0.60),
              decoration: BoxDecoration(
                shape: BoxShape.circle,
                color: Colors.grey.withOpacity(0.05),
              ),
            ),
          ),
          SizedBox(
            width: context.widthPct(0.85),
            height: context.widthPct(0.55),
            child: Column(
              children: [
                const SizedBox(height: 16),
                Padding(
                  padding: const EdgeInsets.symmetric(horizontal: 32.0),
                  child: Row(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    mainAxisAlignment: MainAxisAlignment.spaceBetween,
                    children: [
                      Column(
                        crossAxisAlignment: CrossAxisAlignment.start,
                        children: [
                          Text(
                            'CURRENT BALANCE',
                            textAlign: TextAlign.left,
                            style: Theme.of(context).textTheme.bodyText2,
                          ),
                          const SizedBox(height: 8),
                          Builder(builder: (context) {
                            String rand =
                                Random().nextDouble().toStringAsFixed(6);
                            return Text('AAA $rand',
                                textAlign: TextAlign.left,
                                style: Theme.of(context)
                                    .textTheme
                                    .caption!
                                    .copyWith(
                                        color: AppColors.onSurface
                                            .withOpacity(0.4)));
                          }),
                        ],
                      )
                    ],
                  ),
                ),
                Expanded(child: Container()),
                Padding(
                  padding: const EdgeInsets.symmetric(horizontal: 32),
                  child: Row(
                    mainAxisAlignment: MainAxisAlignment.spaceBetween,
                    children: [
                      Builder(builder: (context) {
                        String amount = (1000 + Random().nextDouble() * 9000)
                            .toStringAsFixed(2);

                        List<String> sides = amount.split('.');

                        String formattedNumber = NumberFormat('0,000')
                            .format(double.parse(sides[0]));

                        return Row(
                          mainAxisAlignment: MainAxisAlignment.center,
                          crossAxisAlignment: CrossAxisAlignment.start,
                          children: [
                            Text(
                              '\$$formattedNumber',
                              style: GoogleFonts.arimo(
                                color: Colors.white,
                                fontSize: 38,
                                fontWeight: FontWeight.w400,
                                letterSpacing: 1,
                              ),
                            ),
                            Text(
                              '.${sides[1]}',
                              style: GoogleFonts.arimo(
                                color: Colors.white,
                                fontSize: 16,
                                fontWeight: FontWeight.w400,
                                letterSpacing: 1,
                              ),
                            ),
                          ],
                        );
                      }),
                      const Icon(
                        Icons.monetization_on,
                        color: AppColors.background,
                        size: 38,
                      )
                    ],
                  ),
                ),
                const SizedBox(height: 24),
              ],
            ),
          ),
        ],
      ),
    );
  }
}
