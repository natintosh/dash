import 'dart:math';

import 'package:dash_demo/shape/account_details_cast_paint.dart';
import 'package:dash_demo/shape/account_details_paint.dart';
import 'package:dash_demo/utils/colors.dart';
import 'package:flutter/material.dart';
import 'package:google_fonts/google_fonts.dart';
import 'package:intl/intl.dart';
import 'package:sized_context/sized_context.dart';

class AccountDetailsCard extends StatelessWidget {
  final VoidCallback onInfoButtonPressed;

  final ValueChanged<bool> onSwitchChanged;

  final bool primaryOn;

  const AccountDetailsCard(
      {Key? key,
      required this.onInfoButtonPressed,
      required this.onSwitchChanged,
      this.primaryOn = false})
      : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Container(
      clipBehavior: Clip.antiAlias,
      width: context.widthPct(0.8),
      height: context.heightPct(0.7),
      decoration: const ShapeDecoration(shape: Border()),
      child: Stack(
        children: [
          Container(
            alignment: Alignment.center,
            child: SizedBox(
              width: context.widthPct(0.8),
              height: context.heightPct(0.7),
              child: CustomPaint(
                painter: AccountDetailsCastPaint(
                    surfaceColor:
                        primaryOn ? AppColors.secondary : AppColors.inActive),
              ),
            ),
          ),
          Container(
            alignment: Alignment.center,
            child: SizedBox(
              width: context.widthPct(0.8),
              height: context.heightPct(0.7),
              child: CustomPaint(
                painter: AccountDetailsPaint(surfaceColor: AppColors.surface),
              ),
            ),
          ),
          Positioned(
            top: -200,
            left: 9,
            child: Container(
              width: context.widthPct(0.8),
              height: context.widthPct(0.8),
              decoration: BoxDecoration(
                shape: BoxShape.circle,
                color: const Color(0xFF353746).withOpacity(0.4),
              ),
            ),
          ),
          Container(
            alignment: Alignment.center,
            child: SizedBox(
              width: context.widthPct(0.8),
              height: context.heightPct(0.7),
              child: Column(
                children: [
                  const SizedBox(height: 4),
                  Row(
                    children: [
                      Expanded(child: Container()),
                      Expanded(
                        flex: 3,
                        child: Text(
                          'BTC ACCOUNT',
                          textAlign: TextAlign.center,
                          style: Theme.of(context).textTheme.caption!.copyWith(
                              color: AppColors.onSurface.withOpacity(0.7)),
                        ),
                      ),
                      Expanded(
                        child: IconButton(
                          onPressed: onInfoButtonPressed,
                          icon: const Icon(Icons.info_outline),
                          iconSize: 16,
                          color: AppColors.onSurface.withOpacity(0.5),
                        ),
                      ),
                    ],
                  ),
                  const SizedBox(height: 24),
                  Builder(builder: (context) {
                    String amount = (1000 + Random().nextDouble() * 9000)
                        .toStringAsFixed(2);

                    List<String> sides = amount.split('.');

                    String formattedNumber =
                        NumberFormat('0,000').format(double.parse(sides[0]));

                    return Row(
                      mainAxisAlignment: MainAxisAlignment.center,
                      crossAxisAlignment: CrossAxisAlignment.start,
                      children: [
                        Text(
                          '\$$formattedNumber',
                          style: GoogleFonts.arimo(
                            color: Colors.white,
                            fontSize: 48,
                            fontWeight: FontWeight.w400,
                            letterSpacing: 1,
                          ),
                        ),
                        Text(
                          '.${sides[1]}',
                          style: GoogleFonts.arimo(
                            color: Colors.white,
                            fontSize: 16,
                            fontWeight: FontWeight.w400,
                            letterSpacing: 1,
                          ),
                        ),
                      ],
                    );
                  }),
                  const SizedBox(height: 20),
                  Text(
                    'Available Amount',
                    textAlign: TextAlign.center,
                    style: Theme.of(context)
                        .textTheme
                        .bodyText2!
                        .copyWith(color: AppColors.onSurface.withOpacity(0.4)),
                  ),
                  const SizedBox(height: 20),
                  const Divider(
                    color: AppColors.background,
                  ),
                  const SizedBox(height: 20),
                  Padding(
                    padding: const EdgeInsets.symmetric(horizontal: 36),
                    child: Row(
                      children: [
                        Text(
                          'Recent Transactions',
                          style: Theme.of(context).textTheme.bodyText2,
                        ),
                        Expanded(child: Container()),
                        Text(
                          'View All',
                          style: Theme.of(context)
                              .textTheme
                              .bodyText2!
                              .copyWith(
                                  color: AppColors.onSurface.withOpacity(0.4)),
                        ),
                        Icon(
                          Icons.arrow_right,
                          color: AppColors.onSurface.withOpacity(0.4),
                        ),
                      ],
                    ),
                  ),
                  const SizedBox(height: 12),
                  Padding(
                    padding: const EdgeInsets.symmetric(horizontal: 36),
                    child: ListView.builder(
                      shrinkWrap: true,
                      itemCount: 4,
                      itemBuilder: (context, index) {
                        return Padding(
                          padding: const EdgeInsets.symmetric(vertical: 12.0),
                          child: Row(
                            children: [
                              Center(
                                child: Container(
                                  width: 32,
                                  height: 32,
                                  decoration: const ShapeDecoration(
                                      color: AppColors.background,
                                      shape: CircleBorder(
                                          side: BorderSide(
                                              color: AppColors.borderColors))),
                                  child: Builder(builder: (context) {
                                    int rand = index + 1;
                                    double? iconSize = 16;
                                    return rand == 1
                                        ? Icon(
                                            Icons.vertical_align_top_sharp,
                                            size: iconSize,
                                          )
                                        : rand == 2
                                            ? Icon(
                                                Icons
                                                    .vertical_align_bottom_sharp,
                                                size: iconSize,
                                              )
                                            : rand == 3
                                                ? Icon(
                                                    Icons.refresh_sharp,
                                                    size: iconSize,
                                                  )
                                                : Icon(
                                                    Icons.close,
                                                    size: iconSize,
                                                  );
                                  }),
                                ),
                              ),
                              const SizedBox(width: 16),
                              Expanded(
                                child: Column(
                                  crossAxisAlignment: CrossAxisAlignment.start,
                                  children: [
                                    Text('Sep 06,2021',
                                        textAlign: TextAlign.left,
                                        style: Theme.of(context)
                                            .textTheme
                                            .bodyText2!
                                            .copyWith(
                                                color: AppColors.onSurface
                                                    .withOpacity(0.4))),
                                    const SizedBox(width: 4),
                                    Text('John Doe',
                                        textAlign: TextAlign.left,
                                        style: Theme.of(context)
                                            .textTheme
                                            .bodyText2),
                                  ],
                                ),
                              ),
                              const SizedBox(width: 16),
                              Builder(builder: (context) {
                                double rand = Random().nextDouble() * 999;

                                String value = rand.toStringAsFixed(2);

                                return Text('\$$value',
                                    textAlign: TextAlign.left,
                                    style:
                                        Theme.of(context).textTheme.bodyText2);
                              }),
                              const Icon(
                                Icons.arrow_right,
                                color: AppColors.secondary,
                              ),
                            ],
                          ),
                        );
                      },
                    ),
                  ),
                  Expanded(child: Container()),
                  Padding(
                    padding: const EdgeInsets.only(left: 24.0, right: 36),
                    child: Align(
                      alignment: Alignment.bottomCenter,
                      child: Row(
                        mainAxisAlignment: MainAxisAlignment.spaceBetween,
                        children: [
                          Row(
                            children: [
                              Text('PRIMARY ON',
                                  style: Theme.of(context)
                                      .textTheme
                                      .caption!
                                      .copyWith(
                                          color: AppColors.onSurface
                                              .withOpacity(0.4))),
                              Switch(
                                value: primaryOn,
                                onChanged: onSwitchChanged,
                                activeColor:
                                    AppColors.onSurface.withOpacity(0.4),
                                activeTrackColor: AppColors.background,
                                inactiveThumbColor:
                                    AppColors.onSurface.withOpacity(0.4),
                                inactiveTrackColor: AppColors.secondary,
                              ),
                            ],
                          ),
                          Text(
                            'ACCOUNT DETAILS',
                            style: primaryOn
                                ? Theme.of(context)
                                    .textTheme
                                    .caption!
                                    .copyWith(color: AppColors.inActive)
                                : Theme.of(context).textTheme.caption!.copyWith(
                                    color:
                                        AppColors.onSurface.withOpacity(0.4)),
                          )
                        ],
                      ),
                    ),
                  ),
                  const SizedBox(height: 4),
                ],
              ),
            ),
          ),
        ],
      ),
    );
  }
}
