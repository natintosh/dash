import 'dart:math';

import 'package:dash_demo/shape/wallet_cast_paint.dart';
import 'package:dash_demo/shape/wallet_paint.dart';
import 'package:dash_demo/utils/colors.dart';
import 'package:dash_demo/utils/widget_view.dart';
import 'package:flutter/material.dart';
import 'package:google_fonts/google_fonts.dart';
import 'package:intl/intl.dart';
import 'package:sized_context/sized_context.dart';

class PrimaryWalletCard extends StatefulWidget {
  const PrimaryWalletCard({Key? key}) : super(key: key);

  @override
  _PrimaryWalletCardState createState() => _PrimaryWalletCardState();
}

class _PrimaryWalletCardState extends State<PrimaryWalletCard> {
  @override
  Widget build(BuildContext context) {
    return _PrimaryWalletCardView(this);
  }
}

class _PrimaryWalletCardView
    extends WidgetView<PrimaryWalletCard, _PrimaryWalletCardState> {
  const _PrimaryWalletCardView(_PrimaryWalletCardState state) : super(state);

  @override
  Widget build(BuildContext context) {
    return Container(
      width: context.widthPct(0.85),
      height: context.widthPct(0.55),
      clipBehavior: Clip.antiAlias,
      decoration: ShapeDecoration(
        shape: RoundedRectangleBorder(
          borderRadius: BorderRadius.circular(context.widthPct(0.85) * 0.11),
        ),
      ),
      child: Stack(
        children: [
          SizedBox(
            width: context.widthPct(0.85),
            height: context.widthPct(0.55),
            child: CustomPaint(
              painter: WalletCastPaint(surfaceColor: AppColors.secondary),
            ),
          ),
          SizedBox(
            width: context.widthPct(0.85),
            height: context.widthPct(0.55),
            child: CustomPaint(
              painter: WalletPaint(surfaceColor: AppColors.surface),
            ),
          ),
          Positioned(
            left: -90,
            top: 90,
            child: Container(
              width: context.widthPct(0.85),
              height: context.widthPct(0.85),
              decoration: BoxDecoration(
                shape: BoxShape.circle,
                color: const Color(0xFF353746).withOpacity(0.4),
              ),
            ),
          ),
          SizedBox(
            width: context.widthPct(0.85),
            height: context.widthPct(0.55),
            child: Column(
              children: [
                const SizedBox(height: 16),
                Padding(
                  padding: const EdgeInsets.symmetric(horizontal: 32.0),
                  child: Row(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    mainAxisAlignment: MainAxisAlignment.spaceBetween,
                    children: [
                      Column(
                        crossAxisAlignment: CrossAxisAlignment.start,
                        children: [
                          Text(
                            'CURRENT BALANCE',
                            textAlign: TextAlign.left,
                            style: Theme.of(context).textTheme.bodyText2,
                          ),
                          const SizedBox(height: 8),
                          Builder(builder: (context) {
                            String rand =
                                Random().nextDouble().toStringAsFixed(6);
                            return Text('BTC $rand',
                                textAlign: TextAlign.left,
                                style: Theme.of(context)
                                    .textTheme
                                    .caption!
                                    .copyWith(
                                        color: AppColors.onSurface
                                            .withOpacity(0.4)));
                          }),
                        ],
                      ),
                      Text(
                        'PRIMARY',
                        textAlign: TextAlign.left,
                        style: Theme.of(context)
                            .textTheme
                            .bodyText2!
                            .copyWith(color: AppColors.onSecondary),
                      ),
                    ],
                  ),
                ),
                Expanded(child: Container()),
                Padding(
                  padding: const EdgeInsets.symmetric(horizontal: 32),
                  child: Row(
                    mainAxisAlignment: MainAxisAlignment.spaceBetween,
                    children: [
                      Builder(builder: (context) {
                        String amount = (1000 + Random().nextDouble() * 9000)
                            .toStringAsFixed(2);

                        List<String> sides = amount.split('.');

                        String formattedNumber = NumberFormat('0,000')
                            .format(double.parse(sides[0]));

                        return Row(
                          mainAxisAlignment: MainAxisAlignment.center,
                          crossAxisAlignment: CrossAxisAlignment.start,
                          children: [
                            Text(
                              '\$$formattedNumber',
                              style: GoogleFonts.arimo(
                                color: Colors.white,
                                fontSize: 38,
                                fontWeight: FontWeight.w400,
                                letterSpacing: 1,
                              ),
                            ),
                            Text(
                              '.${sides[1]}',
                              style: GoogleFonts.arimo(
                                color: Colors.white,
                                fontSize: 16,
                                fontWeight: FontWeight.w400,
                                letterSpacing: 1,
                              ),
                            ),
                          ],
                        );
                      }),
                      const Icon(
                        Icons.monetization_on,
                        color: AppColors.background,
                        size: 38,
                      )
                    ],
                  ),
                ),
                const SizedBox(height: 24),
              ],
            ),
          ),
        ],
      ),
    );
  }
}
