import 'dart:developer' as dev;
import 'dart:math';

import 'package:dash_demo/pages/screens/components/primary_wallet_card.dart';
import 'package:dash_demo/pages/screens/components/wallet_card.dart';
import 'package:dash_demo/provider/card_flip_provider.dart';
import 'package:dash_demo/utils/colors.dart';
import 'package:dash_demo/utils/widget_view.dart';
import 'package:flutter/material.dart';
import 'package:google_fonts/google_fonts.dart';
import 'package:provider/provider.dart';

class DashboardScreen extends StatefulWidget {
  const DashboardScreen({Key? key}) : super(key: key);

  @override
  _DashboardScreenState createState() => _DashboardScreenState();
}

class _DashboardScreenState extends State<DashboardScreen>
    with TickerProviderStateMixin {
  List<String> wallets = ['BTC', 'NAT', 'ABC', 'AAA', 'XYZ'];

  get onMenuButtonPressed => () {};

  late AnimationController forwardAnimationController;
  late AnimationController backwardAnimationController;

  late Animation<double> firstCardRotationAnimation;
  late Animation<Alignment> firstCardTranslationAnimation;
  late Animation<double> secondCardRotationAnimation;
  late Animation<Alignment> secondCardTranslationAnimation;
  late Animation<double> secondCardOpacityAnimation;
  late Animation<double> secondCardScaleAnimation;
  late Animation<Alignment> thirdCardTranslationAnimation;
  late Animation<double> thirdCardOpacityAnimation;
  late Animation<Alignment> fourthCardTranslationAnimation;
  late Animation<double> fourthCardOpacityAnimation;

  late Animation<double> backwardHiddenCardRotationAnimation;
  late Animation<Alignment> backwardHiddenCardTranslationAnimation;
  late Animation<double> backwardFirstCardRotationAnimation;
  late Animation<Alignment> backwardFirstCardTranslationAnimation;
  late Animation<double> backwardFirstCardOpacityAnimation;
  late Animation<double> backwardFirstCardScaleAnimation;
  late Animation<Alignment> backwardSecondCardTranslationAnimation;
  late Animation<double> backwardSecondCardOpacityAnimation;
  late Animation<Alignment> backwardThirdCardTranslationAnimation;
  late Animation<double> backwardThirdCardOpacityAnimation;

  get startAnimation => () {
        context.read<CardFlipProvider>().direction == AnimDirection.forward
            ? forwardAnimationController.forward()
            : backwardAnimationController.forward();
      };

  void initForwardAnimation() {
    firstCardRotationAnimation = Tween<double>(begin: 0, end: -90).animate(
        CurvedAnimation(
            parent: forwardAnimationController,
            curve: const Interval(0, 1, curve: Curves.easeInCubic)));
    firstCardTranslationAnimation = Tween<Alignment>(
            begin: const Alignment(0, -0.5), end: const Alignment(0, -1))
        .animate(CurvedAnimation(
            parent: forwardAnimationController,
            curve: const Interval(0, 0.75, curve: Curves.easeInCubic)));

    secondCardRotationAnimation = Tween<double>(begin: -60, end: 0).animate(
        CurvedAnimation(
            parent: forwardAnimationController,
            curve: const Interval(0, 1, curve: Curves.easeInCubic)));
    secondCardTranslationAnimation = Tween<Alignment>(
            begin: const Alignment(0, 0.75), end: const Alignment(0, -0.5))
        .animate(CurvedAnimation(
            parent: forwardAnimationController,
            curve: const Interval(0, 1, curve: Curves.easeInCubic)));
    secondCardOpacityAnimation = Tween<double>(begin: 0.5, end: 1).animate(
        CurvedAnimation(
            parent: forwardAnimationController,
            curve: const Interval(0, 1, curve: Curves.easeInCubic)));
    secondCardScaleAnimation = Tween<double>(begin: 0.92, end: 1).animate(
        CurvedAnimation(
            parent: forwardAnimationController,
            curve: const Interval(0, 1, curve: Curves.easeInCubic)));

    thirdCardTranslationAnimation = Tween<Alignment>(
            begin: const Alignment(0, 1.15), end: const Alignment(0, 0.75))
        .animate(CurvedAnimation(
            parent: forwardAnimationController,
            curve: const Interval(0.25, 1, curve: Curves.easeInCubic)));

    thirdCardOpacityAnimation = Tween<double>(begin: 0.25, end: 0.5).animate(
        CurvedAnimation(
            parent: forwardAnimationController,
            curve: const Interval(0.25, 1, curve: Curves.easeInCubic)));

    fourthCardTranslationAnimation = Tween<Alignment>(
            begin: const Alignment(0, 1.5), end: const Alignment(0, 1.15))
        .animate(CurvedAnimation(
            parent: forwardAnimationController,
            curve: const Interval(0.25, 1, curve: Curves.easeInCubic)));

    fourthCardOpacityAnimation = Tween<double>(begin: 0, end: 0.25).animate(
        CurvedAnimation(
            parent: forwardAnimationController,
            curve: const Interval(0.25, 1, curve: Curves.easeInCubic)));
  }

  void initBackwardAnimation() {
    backwardHiddenCardRotationAnimation = Tween<double>(begin: -90, end: 0)
        .animate(CurvedAnimation(
            parent: backwardAnimationController,
            curve: const Interval(0.25, 1, curve: Curves.easeInCubic)));
    backwardHiddenCardTranslationAnimation = Tween<Alignment>(
            begin: const Alignment(0, -1.5), end: const Alignment(0, -0.5))
        .animate(CurvedAnimation(
            parent: backwardAnimationController,
            curve: const Interval(0, 1, curve: Curves.easeInCubic)));

    backwardFirstCardRotationAnimation = Tween<double>(begin: 0, end: -60)
        .animate(CurvedAnimation(
            parent: backwardAnimationController,
            curve: const Interval(0.25, 1, curve: Curves.easeInCubic)));
    backwardFirstCardTranslationAnimation = Tween<Alignment>(
            begin: const Alignment(0, -0.5), end: const Alignment(0, 0.75))
        .animate(CurvedAnimation(
            parent: backwardAnimationController,
            curve: const Interval(0.25, 1, curve: Curves.easeInCubic)));
    backwardFirstCardOpacityAnimation = Tween<double>(begin: 1, end: 0.5)
        .animate(CurvedAnimation(
            parent: backwardAnimationController,
            curve: const Interval(0.25, 1, curve: Curves.easeInCubic)));
    backwardFirstCardScaleAnimation = Tween<double>(begin: 1, end: 0.92)
        .animate(CurvedAnimation(
            parent: backwardAnimationController,
            curve: const Interval(0.25, 1, curve: Curves.easeInCubic)));

    backwardSecondCardTranslationAnimation = Tween<Alignment>(
            begin: const Alignment(0, 0.75), end: const Alignment(0, 1.15))
        .animate(CurvedAnimation(
            parent: backwardAnimationController,
            curve: const Interval(0.25, 1, curve: Curves.easeInCubic)));

    backwardSecondCardOpacityAnimation = Tween<double>(begin: 0.5, end: 0.25)
        .animate(CurvedAnimation(
            parent: backwardAnimationController,
            curve: const Interval(0.25, 1, curve: Curves.easeInCubic)));

    backwardThirdCardTranslationAnimation = Tween<Alignment>(
            begin: const Alignment(0, 1.15), end: const Alignment(0, 1.5))
        .animate(CurvedAnimation(
            parent: backwardAnimationController,
            curve: const Interval(0.25, 1, curve: Curves.easeInCubic)));

    backwardThirdCardOpacityAnimation = Tween<double>(begin: 0.25, end: 0)
        .animate(CurvedAnimation(
            parent: backwardAnimationController,
            curve: const Interval(0.25, 1, curve: Curves.easeInCubic)));
  }

  void forwardAnimationListener() {
    if (forwardAnimationController.isCompleted) {
      context.read<CardFlipProvider>().index =
          context.read<CardFlipProvider>().animatingIndex;
      forwardAnimationController.reset();
    }
  }

  void backwardAnimationListener() {
    if (backwardAnimationController.isCompleted) {
      context.read<CardFlipProvider>().index =
          context.read<CardFlipProvider>().animatingIndex;
      backwardAnimationController.reset();
    }
  }

  void onVerticalDragUpdate(DragUpdateDetails details) {
    if (details.primaryDelta!.isNegative) {
      context.read<CardFlipProvider>().direction = AnimDirection.forward;
    } else {
      context.read<CardFlipProvider>().direction = AnimDirection.backward;
    }
  }

  void onVerticalDragEnd(DragEndDetails details) {
    if (!backwardAnimationController.isAnimating &&
        !forwardAnimationController.isAnimating) {
      bool isForward =
          context.read<CardFlipProvider>().direction == AnimDirection.forward;

      if (isForward) {
        forwardAnimation();
      } else {
        backwardAnimation();
      }
    }
  }

  void onVerticalDragStart(DragStartDetails details) {}

  void forwardAnimation() {
    if (context.read<CardFlipProvider>().index >
        context.read<CardFlipProvider>().data.length - 1) {
      return;
    }

    backwardAnimationController.stop();

    Provider.of<CardFlipProvider>(context, listen: false).animatingIndex++;

    forwardAnimationController.forward(from: backwardAnimationController.value);
    backwardAnimationController.reset();
  }

  void backwardAnimation() {
    if (context.read<CardFlipProvider>().index < 1) {
      return;
    }

    forwardAnimationController.stop();

    Provider.of<CardFlipProvider>(context, listen: false).animatingIndex--;

    backwardAnimationController.forward(from: forwardAnimationController.value);
    forwardAnimationController.reset();
  }

  @override
  void initState() {
    forwardAnimationController = AnimationController(
        duration: const Duration(milliseconds: 500), vsync: this)
      ..addListener((forwardAnimationListener));
    backwardAnimationController = AnimationController(
        duration: const Duration(milliseconds: 500), vsync: this)
      ..addListener((backwardAnimationListener));

    initForwardAnimation();
    initBackwardAnimation();
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return _DashboardScreenView(this);
  }
}

class _DashboardScreenView
    extends WidgetView<DashboardScreen, _DashboardScreenState> {
  const _DashboardScreenView(_DashboardScreenState state) : super(state);

  @override
  Widget build(BuildContext context) {
    dev.log("${context.watch<CardFlipProvider>().index}");
    return Column(
      children: [
        const SizedBox(height: 4),
        Row(
          children: [
            const SizedBox(width: 16),
            IconButton(
              onPressed: state.onMenuButtonPressed,
              icon: const Icon(
                Icons.menu,
                color: AppColors.secondary,
              ),
            ),
            Expanded(
              child: Container(),
            ),
            IconButton(
                onPressed: state.onMenuButtonPressed,
                icon: Icon(
                  Icons.loop,
                  color: AppColors.onSurface.withOpacity(0.5),
                )),
            const SizedBox(width: 16),
          ],
        ),
        const SizedBox(height: 36),
        Padding(
          padding: const EdgeInsets.symmetric(horizontal: 36),
          child: Container(
            alignment: Alignment.centerLeft,
            child: Text(
              'Good day Peter,\nWelcome to your\nwallet',
              textAlign: TextAlign.left,
              style: GoogleFonts.arimoTextTheme(Theme.of(context).textTheme)
                  .headline5!
                  .copyWith(wordSpacing: 1, letterSpacing: 1.2),
            ),
          ),
        ),
        const SizedBox(
          height: 36,
        ),
        Expanded(
          child: GestureDetector(
            onVerticalDragStart: state.onVerticalDragStart,
            onVerticalDragEnd: state.onVerticalDragEnd,
            onVerticalDragUpdate: state.onVerticalDragUpdate,
            child: AnimatedBuilder(
              animation: context.watch<CardFlipProvider>().direction ==
                      AnimDirection.forward
                  ? state.forwardAnimationController
                  : state.backwardAnimationController,
              child: Container(),
              builder: (context, child) {
                bool isForward = context.watch<CardFlipProvider>().direction ==
                    AnimDirection.forward;

                double fourthCardScale = 0.92;
                double fourthCardRotation = -60 * pi / 180;
                double fourthCardOpacity =
                    state.fourthCardOpacityAnimation.value;
                Alignment fourthCardAlignment =
                    state.fourthCardTranslationAnimation.value;

                Alignment thirdCardAlignment = isForward
                    ? state.thirdCardTranslationAnimation.value
                    : state.backwardThirdCardTranslationAnimation.value;
                double thirdCardRotation = -60 * pi / 180;
                double thirdCardScale = 0.92;
                double thirdCardOpacity = isForward
                    ? state.thirdCardOpacityAnimation.value
                    : state.backwardThirdCardOpacityAnimation.value;

                Alignment secondCardAlignment = isForward
                    ? state.secondCardTranslationAnimation.value
                    : state.backwardSecondCardTranslationAnimation.value;
                double secondCardRotation =
                    state.secondCardRotationAnimation.value * pi / 180;
                double secondCardScale = state.secondCardScaleAnimation.value;
                double secondCardOpacity = isForward
                    ? state.secondCardOpacityAnimation.value
                    : state.backwardSecondCardOpacityAnimation.value;

                Alignment firstCardAlignment = isForward
                    ? state.firstCardTranslationAnimation.value
                    : state.backwardFirstCardTranslationAnimation.value;
                double firstCardRotation = isForward
                    ? state.firstCardRotationAnimation.value * pi / 180
                    : state.backwardFirstCardRotationAnimation.value * pi / 180;
                double firstCardOpacity = isForward
                    ? 1
                    : state.backwardFirstCardOpacityAnimation.value;

                Alignment zeroCardAlignment = isForward
                    ? const Alignment(0, -1)
                    : state.backwardHiddenCardTranslationAnimation.value;
                double zeroCardOpacity = isForward ? 0 : 1;
                double zeroCardRotation = isForward
                    ? -90 * pi / 180
                    : state.backwardHiddenCardRotationAnimation.value *
                        pi /
                        180;

                return Stack(
                  children: [
                    Container(
                      alignment: fourthCardAlignment,
                      child: Transform(
                        transform: Matrix4.identity()
                          ..setEntry(3, 2, 0.001)
                          ..rotateX(fourthCardRotation)
                          ..scale(fourthCardScale),
                        alignment: FractionalOffset.center,
                        child: Opacity(
                          opacity: fourthCardOpacity,
                          child: const WalletCard(),
                        ),
                      ),
                    ),
                    Container(
                      alignment: thirdCardAlignment,
                      child: Transform(
                        transform: Matrix4.identity()
                          ..setEntry(3, 2, 0.001)
                          ..rotateX(thirdCardRotation)
                          ..scale(thirdCardScale),
                        alignment: FractionalOffset.center,
                        child: Opacity(
                          opacity: thirdCardOpacity,
                          child: const WalletCard(),
                        ),
                      ),
                    ),
                    Container(
                      alignment: secondCardAlignment,
                      child: Transform(
                        transform: Matrix4.identity()
                          ..setEntry(3, 2, 0.001)
                          ..rotateX(secondCardRotation)
                          ..scale(secondCardScale),
                        alignment: FractionalOffset.center,
                        child: Opacity(
                          opacity: secondCardOpacity,
                          child: const WalletCard(),
                        ),
                      ),
                    ),
                    Container(
                      alignment: firstCardAlignment,
                      child: Transform(
                          alignment: FractionalOffset.center,
                          transform: Matrix4.identity()
                            ..setEntry(3, 2, 0.001)
                            ..rotateX(firstCardRotation),
                          child: Opacity(
                              opacity: firstCardOpacity,
                              child: context.read<CardFlipProvider>().index == 0
                                  ? const PrimaryWalletCard()
                                  : const WalletCard())),
                    ),
                    Container(
                      alignment: zeroCardAlignment,
                      child: Transform(
                          alignment: FractionalOffset.center,
                          transform: Matrix4.identity()
                            ..setEntry(3, 2, 0.001)
                            ..rotateX(zeroCardRotation),
                          child: Opacity(
                              opacity: zeroCardOpacity,
                              child: const WalletCard())),
                    )
                  ],
                );
              },
            ),
          ),
        ),
        const SizedBox(height: 36),
        Container(
          alignment: Alignment.center,
          child: Text(
            'Tap to Pay',
            textAlign: TextAlign.center,
            style: GoogleFonts.arimoTextTheme(Theme.of(context).textTheme)
                .bodyText2!
                .copyWith(color: AppColors.onSurface.withOpacity(0.7)),
          ),
        ),
        Container(
          alignment: Alignment.center,
          child: Text(
            'Last Update 02/09/2021',
            textAlign: TextAlign.center,
            style: GoogleFonts.arimoTextTheme(Theme.of(context).textTheme)
                .caption!
                .copyWith(color: AppColors.onSurface.withOpacity(0.4)),
          ),
        ),
        const SizedBox(height: 36),
      ],
    );
  }
}
