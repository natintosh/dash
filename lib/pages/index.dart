import 'dart:developer';

import 'package:dash_demo/pages/screens/accounts_view.dart';
import 'package:dash_demo/pages/screens/dashboard_view.dart';
import 'package:dash_demo/provider/index_provider.dart';
import 'package:dash_demo/utils/widget_view.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:provider/provider.dart';
import 'package:rive/rive.dart';

class IndexPage extends StatefulWidget {
  const IndexPage({Key? key}) : super(key: key);

  @override
  _IndexPageState createState() => _IndexPageState();
}

class _IndexPageState extends State<IndexPage> {
  @override
  Widget build(BuildContext context) {
    return SafeArea(
      top: true,
      bottom: false,
      child: _IndexPageView(this),
    );
  }

  void onBottomNavigationItemTapped(int value) {
    context.read<IndexProvider>().index = value;
  }
}

class _IndexPageView extends WidgetView<IndexPage, _IndexPageState> {
  const _IndexPageView(_IndexPageState state) : super(state);

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: AnimatedSwitcher(
        duration: const Duration(milliseconds: 400),
        child: context.watch<IndexProvider>().index % 2 == 1
            ? const AccountsScreen()
            : const DashboardScreen(),
      ),
      bottomNavigationBar: BottomNavigationBar(
        currentIndex: context.watch<IndexProvider>().index,
        onTap: state.onBottomNavigationItemTapped,
        type: BottomNavigationBarType.fixed,
        showSelectedLabels: false,
        showUnselectedLabels: false,
        items: const [
          BottomNavigationBarItem(
            icon: IconIndicator(
              index: 0,
            ),
            label: 'Notifications',
          ),
          BottomNavigationBarItem(
            icon: IconIndicator(
              index: 1,
            ),
            label: 'Payment',
          ),
          BottomNavigationBarItem(
            icon: IconIndicator(
              index: 2,
            ),
            label: 'Dashboard',
          ),
          BottomNavigationBarItem(
            icon: IconIndicator(
              index: 3,
            ),
            label: 'Gallery',
          ),
          BottomNavigationBarItem(
            icon: IconIndicator(
              index: 4,
            ),
            label: 'Settings',
          ),
        ],
      ),
    );
  }
}

class IconIndicator extends StatefulWidget {
  final int index;
  const IconIndicator({Key? key, required this.index}) : super(key: key);

  @override
  _IconIndicatorState createState() => _IconIndicatorState();
}

class _IconIndicatorState extends State<IconIndicator> {
  Artboard? artboard;

  StateMachineController? stateMachineController;

  late SimpleAnimation active, inactive;
  void initializeRiveAnimation() async {
    try {
      final ByteData data = await rootBundle.load('assets/rive/icon.riv');

      final RiveFile riveFile = RiveFile.import(data);

      final Artboard artboard = riveFile.mainArtboard;

      inactive = SimpleAnimation('Inactive');
      active = SimpleAnimation('Active');

      if (context.read<IndexProvider>().index == widget.index) {
        artboard.addController(active);
      } else {
        artboard.addController(inactive);
      }
      setState(() {
        this.artboard = artboard;
      });
    } on Exception catch (e) {
      log('$e');
    }
  }

  void indexListener() {
    log('Listening');
    if (context.read<IndexProvider>().index == widget.index) {
      artboard!.removeController(inactive);
      artboard!.addController(active);
    } else {
      artboard!.removeController(active);
      artboard!.addController(inactive);
    }
  }

  @override
  void initState() {
    initializeRiveAnimation();
    super.initState();
    WidgetsBinding.instance!.addPostFrameCallback((timeStamp) {
      context.read<IndexProvider>().addListener(indexListener);
    });
  }

  @override
  Widget build(BuildContext context) {
    return SizedBox(
      height: 24,
      width: 24,
      child: artboard == null
          ? Container()
          : Rive(
              artboard: artboard!,
            ),
    );
  }
}
