import 'package:dash_demo/utils/colors.dart';
import 'package:flutter/material.dart';

class AppTheme {
  static ThemeData get darkTheme => ThemeData.dark().copyWith(
      visualDensity: VisualDensity.adaptivePlatformDensity,
      pageTransitionsTheme: const PageTransitionsTheme(
        builders: <TargetPlatform, PageTransitionsBuilder>{
          TargetPlatform.android: ZoomPageTransitionsBuilder(),
          TargetPlatform.iOS: CupertinoPageTransitionsBuilder()
        },
      ),
      colorScheme: darkColorScheme,
      scaffoldBackgroundColor: AppColors.background,
      bottomNavigationBarTheme: const BottomNavigationBarThemeData(
        backgroundColor: AppColors.background,
      ));

  static ColorScheme darkColorScheme = const ColorScheme.dark(
    primary: AppColors.primary,
    primaryVariant: AppColors.primaryVariant,
    secondary: AppColors.secondary,
    secondaryVariant: AppColors.secondaryVariant,
    background: AppColors.background,
    surface: AppColors.surface,
    onPrimary: AppColors.onPrimary,
    onSecondary: AppColors.onSecondary,
    onBackground: AppColors.background,
    onSurface: AppColors.onSurface,
    error: AppColors.error,
    onError: AppColors.onError,
  );
}
