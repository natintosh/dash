import 'package:flutter/material.dart';

class AppColors {
  AppColors._();

  static const Color primary = Color(0xFF353746);

  static const Color primaryVariant = Color(0xFF494B5B);

  static const Color secondary = Color(0xFFFD9401);

  static const Color secondaryVariant = Color(0xFFF37802);

  static const Color surface = Color(0xFF494B5B);

  static const Color background = Color(0xFF353746);

  static const Color error = Color(0xFFBA1B1B);

  static const Color onPrimary = Color(0xffE0E0E2);

  static const Color onSecondary = Color(0xFF0F111D);

  static const Color onSurface = Color(0xFFE0E0E2);

  static const Color onBackground = Color(0xFFE0E0E2);

  static const Color onError = Color(0xFFE0E0E2);

  static const Color unSelectedItemColor = Color(0xFF575968);

  static const Color borderColors = Color(0xFF262836);

  static const Color inActive = Color(0xFF232330);
}
